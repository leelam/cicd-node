export type RequestMethod = 'get' | 'post' | 'delete' | 'options' | 'put';
export interface IRouteDefinition {
  // Path  route
  path: string;
  // method
  requestMethod: RequestMethod;
  // method for controller handler
  methodName: string | symbol;
}