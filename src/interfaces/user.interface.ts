export interface User {
    id?: string;
    first_name: string;
    last_name: string;
    password: string;
    create_at: Date;
}