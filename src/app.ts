import express, { Application, Request, Response } from 'express';
import cors from 'cors';
import path from 'path';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import dotenv from "dotenv";
import "reflect-metadata";
import { createConnection } from 'typeorm'
import { router } from './router/index'
import './api'
import mongoConnect from './database/mongod.connect';


export class App {
    app: Application;

    constructor(private port?: number | string) {
        this.app = express();
        dotenv.config()
        this.settings();
        this.parser();
        this.connectSql();
        this.connectMongo();
        this.routes();
    }
    routes() {
        router(this.app)
    }
    settings() {
        this.app.set('port', this.port || process.env.PORT || 5432)
    }
    parser() {
        const corsOptions = {
            "origin": "*",
            "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
            "preflightContinue": false,
            "optionsSuccessStatus": 204
          }
        this.app.use(cookieParser());
        this.app.use(cors(corsOptions));
        this.app.use(express.static(path.join(__dirname, '../resources')));
        this.app.use(bodyParser.json());
        this.app.use(
            bodyParser.urlencoded({
                extended: true,
            })
        );
    }

    connectSql() {
        createConnection()
    }

    connectMongo() {
       mongoConnect('mongodb://asset_test:supertest@192.168.1.254:27018/asset_test?authSource=admin')
    }

    async listen(){
        await this.app.listen(this.app.get('port'));
        console.log(`Server running on port: ${this.app.get('port')}`);
    }
}