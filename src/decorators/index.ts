import { Controller } from './controller';
import { Get } from './get';
import { Post } from './post'
import { Put } from './put'

export {
    Controller,
    Get,
    Post,
    Put
}
