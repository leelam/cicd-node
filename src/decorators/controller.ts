
export const Controller = (prefix: string): ClassDecorator => {
	return (target) => {
		Reflect.defineMetadata('prefix', prefix, target);
		
		// Controller without router (impossible)
		if (!Reflect.hasMetadata('routes', target)) {
			Reflect.defineMetadata('routes', [], target);
		}
	}
};