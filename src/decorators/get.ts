import { IRouteDefinition } from "../interfaces/router.definition";
export const Get = (path: string): MethodDecorator => {
  // `target` là class của chúng ta - Controller, `propertyKey` tương ứng với tên phương thức - để xử lý request
  return (target, propertyKey) => {
    // Trong trường hợp đây là lần đầu `routes` được đăng ký, thông tin `routes` sẽ là undefined.
    // Để các bước sau có thể hoạt động, đơn giản set giá trị cho `routes` là một mảng rỗng.
    if (!Reflect.hasMetadata('routes', target.constructor)) {
      Reflect.defineMetadata('routes', [], target.constructor);
    }
    // Lấy giá trị routes đã được lưu trước đó, thêm vào một route mới và set lại vào metadata.
    const routes: IRouteDefinition[] = Reflect.getMetadata('routes', target.constructor);
    routes.push({
      requestMethod: 'get',
      path,
      methodName: propertyKey,
    });
    Reflect.defineMetadata('routes', routes, target.constructor);
  };
};