import { get, trim } from 'lodash';

module.exports = require(`./${trim(
  get(process, 'env.NODE_ENV', 'development')
)}.config`);