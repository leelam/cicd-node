import mongoose, { Schema, Document } from 'mongoose';

export interface IBook extends Document {
    name: string;
}

const BookSchema: Schema = new Schema({
    name: { type: String, required: true },
    create_date: {type: Date, default: Date.now}
})

export default mongoose.model<IBook>('Book', BookSchema)