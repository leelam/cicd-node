import Book, { IBook } from '../schemas/book';
import { BookData } from '../interfaces/book.interface';
 
const createBook = async ({name, create_date}: BookData): Promise<IBook> => {
    const newBook = await new Book({
        name
    }).save()
    return newBook;
}

const listBook = async () => {
    const result = await Book.find().lean();
    return result;
}

export {
    createBook,
    listBook
}