import crypto from 'crypto';
const key =  {
    key: 'D9048B2CA883DF19',
    pre: 'A',
    after: 'C5',
  }

const algorithm = 'aes-128-cbc';
const iv = crypto.randomBytes(16);
function encrypt(text: any) {
  let cipher = crypto.createCipheriv(algorithm, Buffer.from(key), iv);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex') };
}

module.exports = encrypt;