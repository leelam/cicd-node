import { Request, Response, Router } from 'express';
import { Get, Controller, Post, Put } from '../decorators/index'
import { createBook, listBook } from '../controllers/book.controller';

@Controller('/book')
export default class Books {
    public router = Router();
    @Put('/create')
    async createBook(req: Request, res: Response) {
        const result = await createBook({name: req.body.name, create_date: Date.now()})
        return res.status(200).json(result)
    }
    @Get('/list')
    async getListBook(req: Request, res: Response){
        const result = await listBook()
        return res.status(200).json({
            data: result
        })
    }
}