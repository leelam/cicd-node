import { Request, Response, Router } from 'express';
import { Get, Controller, Post, Put } from '../decorators/index'
import { User } from '../models/User';
import { getRepository } from 'typeorm'

@Controller('/user')
export default class Users {
    public router = Router();
    @Get('/')
    getMessage(req: Request, res: Response) {
        return res.status(200).json({
            message: 'hehe'
        });
    }
    @Get('/list')
    async getList(req: Request, res: Response) {
        const listUser = await getRepository(User).find();
        return res.status(200).json({
            data: listUser
        })
    }
    @Post('/add')
    async addUser(req: Request, res: Response) {
        try {
            const data = req.body;
            let newUser = new User();
            newUser = { ...data }
            let userRepository = getRepository(User);
            await userRepository.save(newUser)
            return res.status(200).json({
                data: newUser
            })
        } catch (error) {
            return res.status(502).json({
                message: 'Server Error'
            })
        }
    }
    @Put('/update/:id')
    async updateUser(req: Request, res: Response) {
        try {
            const data = req.body;
            const id = req.params.id;
            let userRepository = getRepository(User);
            let userUpDate = await userRepository.findOne(id);
            if (userUpDate) {
                Object.assign(userUpDate,data);
                await userRepository.save(userUpDate);
                return res.status(200).json({
                    data: userUpDate
                })
            } return res.status(404).json({
                massge: 'User not found!'
            })
            
        } catch (error) {
            return res.status(502).json({
                message: 'Server Error'
            })
        }
    }

}