process.env.NODE_ENV = 'test';
const should  = require('chai').should();
const expect  = require('chai').expect;
var request = require('request');

it('Main page content', function(done) {
    request('http://localhost:5432' , function(error, response, body) {
        should.exist(body);
        const data = JSON.parse(body);
        console.log(data);
        expect(data.status).to.be.true;
        done();
    });
});