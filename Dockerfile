FROM node:12
LABEL maintainer "16520637@gm.uit.edu.vn"
HEALTHCHECK --interval=5s \
            --timeout=5s \
            CMD curl -f http://127.0.0.1:8000 || exit 1

# tell docker what port to expose
EXPOSE 8000