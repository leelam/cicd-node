"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var index_1 = require("../decorators/index");
var User_1 = require("../models/User");
var typeorm_1 = require("typeorm");
var Users = /** @class */ (function () {
    function Users() {
        this.router = express_1.Router();
    }
    Users.prototype.getMessage = function (req, res) {
        return res.status(200).json({
            message: 'hehe'
        });
    };
    Users.prototype.getList = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var listUser;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, typeorm_1.getRepository(User_1.User).find()];
                    case 1:
                        listUser = _a.sent();
                        return [2 /*return*/, res.status(200).json({
                                data: listUser
                            })];
                }
            });
        });
    };
    Users.prototype.addUser = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var data, newUser, userRepository, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        data = req.body;
                        newUser = new User_1.User();
                        newUser = __assign({}, data);
                        userRepository = typeorm_1.getRepository(User_1.User);
                        return [4 /*yield*/, userRepository.save(newUser)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, res.status(200).json({
                                data: newUser
                            })];
                    case 2:
                        error_1 = _a.sent();
                        return [2 /*return*/, res.status(502).json({
                                message: 'Server Error'
                            })];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    Users.prototype.updateUser = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var data, id, userRepository, userUpDate, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        data = req.body;
                        id = req.params.id;
                        userRepository = typeorm_1.getRepository(User_1.User);
                        return [4 /*yield*/, userRepository.findOne(id)];
                    case 1:
                        userUpDate = _a.sent();
                        if (!userUpDate) return [3 /*break*/, 3];
                        Object.assign(userUpDate, data);
                        return [4 /*yield*/, userRepository.save(userUpDate)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, res.status(200).json({
                                data: userUpDate
                            })];
                    case 3: return [2 /*return*/, res.status(404).json({
                            massge: 'User not found!'
                        })];
                    case 4:
                        error_2 = _a.sent();
                        return [2 /*return*/, res.status(502).json({
                                message: 'Server Error'
                            })];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        index_1.Get('/'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object]),
        __metadata("design:returntype", void 0)
    ], Users.prototype, "getMessage", null);
    __decorate([
        index_1.Get('/list'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object]),
        __metadata("design:returntype", Promise)
    ], Users.prototype, "getList", null);
    __decorate([
        index_1.Post('/add'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object]),
        __metadata("design:returntype", Promise)
    ], Users.prototype, "addUser", null);
    __decorate([
        index_1.Put('/update/:id'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object]),
        __metadata("design:returntype", Promise)
    ], Users.prototype, "updateUser", null);
    Users = __decorate([
        index_1.Controller('/user')
    ], Users);
    return Users;
}());
exports.default = Users;
