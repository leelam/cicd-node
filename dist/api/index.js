"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BookApi = exports.UserApi = void 0;
var user_api_1 = __importDefault(require("./user.api"));
exports.UserApi = user_api_1.default;
var book_api_1 = __importDefault(require("./book.api"));
exports.BookApi = book_api_1.default;
