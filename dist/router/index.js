"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
var index_1 = require("../api/index");
exports.router = function (app) {
    [
        index_1.UserApi,
        index_1.BookApi
    ].forEach(function (controller) {
        // Khởi tạo đối tượng controller
        var instance = new controller();
        // Lấy thông tin của prefix, chúng ta đã lưu chúng trong metadata của class controller
        var prefix = Reflect.getMetadata('prefix', controller);
        // Tương tự, lất ra tất cả các `routes`
        var routes = Reflect.getMetadata('routes', controller);
        // Duyệt qua tất cả các routes và đăng ký chúng với express
        routes.forEach(function (route) {
            // Ở đây, tốt nhất là dùng `switch/case` để đảm bảo chúng ta sử dụng đúng phương thức của express(.get, .post(), ...)
            // Nhưng để đơn giản thì như thế này là đủ
            app[route.requestMethod](prefix + route.path, function (req, res) {
                // Thực thi phương thức xử lý request, truyền vào là request và response
                instance[route.methodName](req, res);
            });
        });
    });
};
