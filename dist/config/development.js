"use strict";
module.exports = {
    database: {
        mongoUrl: 'mongodb://192.168.1.254:27018,192.168.1.254:27019/asset_test?replicaSet=asset_test1',
        elasticsearchHost: 'http://192.168.1.254:9200',
        elasticsearchSyncHost: '192.168.1.254',
        MONGOUSER: 'asset_test',
        MONGOPASSWORD: 'supertest',
    },
};
