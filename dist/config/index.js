"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var lodash_1 = require("lodash");
module.exports = require("./" + lodash_1.trim(lodash_1.get(process, 'env.NODE_ENV', 'development')) + ".config");
