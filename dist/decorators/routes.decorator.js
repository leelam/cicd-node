"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.appRouter = void 0;
var express_1 = require("express");
exports.appRouter = express_1.Router();
function routesDecorator(options) {
    return function (target, propertyKey, descriptor) {
        exports.appRouter[options.method](options.path, target[propertyKey]);
    };
}
exports.default = routesDecorator;
