"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Post = void 0;
exports.Post = function (path) {
    // `target` là class của chúng ta - Controller, `propertyKey` tương ứng với tên phương thức - để xử lý request
    return function (target, propertyKey) {
        // Trong trường hợp đây là lần đầu `routes` được đăng ký, thông tin `routes` sẽ là undefined.
        // Để các bước sau có thể hoạt động, đơn giản set giá trị cho `routes` là một mảng rỗng.
        if (!Reflect.hasMetadata('routes', target.constructor)) {
            Reflect.defineMetadata('routes', [], target.constructor);
        }
        // Lấy giá trị routes đã được lưu trước đó, thêm vào một route mới và set lại vào metadata.
        var routes = Reflect.getMetadata('routes', target.constructor);
        routes.push({
            requestMethod: 'post',
            path: path,
            methodName: propertyKey,
        });
        Reflect.defineMetadata('routes', routes, target.constructor);
    };
};
