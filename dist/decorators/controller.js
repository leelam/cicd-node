"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Controller = void 0;
exports.Controller = function (prefix) {
    return function (target) {
        Reflect.defineMetadata('prefix', prefix, target);
        // Controller without router (impossible)
        if (!Reflect.hasMetadata('routes', target)) {
            Reflect.defineMetadata('routes', [], target);
        }
    };
};
