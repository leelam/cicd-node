"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var crypto_1 = __importDefault(require("crypto"));
var key = {
    key: 'D9048B2CA883DF19',
    pre: 'A',
    after: 'C5',
};
var algorithm = 'aes-128-cbc';
var iv = crypto_1.default.randomBytes(16);
function encrypt(text) {
    var cipher = crypto_1.default.createCipheriv(algorithm, Buffer.from(key), iv);
    var encrypted = cipher.update(text);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex') };
}
module.exports = encrypt;
